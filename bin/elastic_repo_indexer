#!/usr/bin/env ruby

require 'rubygems'
require 'bundler/setup'
require 'json'
require 'elasticsearch/git'
require 'active_support'
require 'active_support/core_ext'
require 'benchmark'

path_to_log_file = File.expand_path('../../log/es-indexer.log', __FILE__)
LOGGER = Logger.new(path_to_log_file)

PROJECT_ID = ARGV.shift
REPO_PATH = ARGV.shift
FROM_SHA = ENV['FROM_SHA']
TO_SHA = ENV['TO_SHA']
RAILS_ENV = ENV['RAILS_ENV']

LOGGER.info("[ES indexer]: Has been scheduled for project #{REPO_PATH} with SHA range #{FROM_SHA}:#{TO_SHA}")

elastic_connection_info = JSON.parse ENV['ELASTIC_CONNECTION_INFO']
ELASTIC_HOST = elastic_connection_info['host']
ELASTIC_PORT = elastic_connection_info['port']

class Repository
  include Elasticsearch::Git::Repository

  index_name ['gitlab', RAILS_ENV].compact.join('-')

  self.__elasticsearch__.client = Elasticsearch::Client.new(
    host: ELASTIC_HOST,
    port: ELASTIC_PORT
  )

  def client_for_indexing
    self.__elasticsearch__.client
  end

  def repository_id
    PROJECT_ID
  end

  def path_to_repo
    REPO_PATH
  end
end

repo = Repository.new

params = { from_rev: FROM_SHA, to_rev: TO_SHA }.compact

print "Indexing commits..."

timings = Benchmark.measure do
  repo.index_commits(params)
end

puts "Done"

LOGGER.info("[ES indexer]: Commits for #{REPO_PATH} are indexed. Time elapsed: #{timings.real}")

print "Indexing blobs..."

timings = Benchmark.measure do
  repo.index_blobs(params)
end

puts "Done"

LOGGER.info("[ES indexer]: Blobs for #{REPO_PATH} are indexed. Time elapsed: #{timings.real}")
